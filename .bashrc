#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

### RANDOM COLOR SCRIPT ###
colorscript random

alias reboot='sudo reboot'
alias poweroff='sudo poweroff'



