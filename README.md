# arch-dwm-layla

![SCROT](2022-06-16-210546_1920x1080_scrot.png)

My config files for Arch linux+dwm window manager

# info
+ After a recent new machine build and install of Arch linux with dwm window manager I decided to archive my config files.
+ https://layla.lu/new-machine-1-t-11-years/

dwm minimal patching
+ alpha
+ no borders
+ clientindicators
+ https://dwm.suckless.org/patches/

Dracula theme
+ gtk
+ dwm/dmenu
+ .Xresources
+ https://draculatheme.com/

notes
+ librewolf
+ picom for transparency
+ mpd/mpc/ncmpcpp for audio streaming (pulseaudio)
+ slstatus with mpd now playing script
+ ohsnap fonts
+ terminess ttf nerd font
+ urxvtc terminal
+ viewnior
+ bpytop resource monitor
+ cpufetch
+ archey
+ xfce-polkit for mounting drives ( /usr/lib/xfce-polkit/xfce-polkit in ~/.xinitrc)
+ binclock.sh just for fun
+ D.T. shell color script in ~/.bashrc (yay -S shell-color-scripts )
+ https://gitlab.com/dwt1/shell-color-scripts

+ 17 June 2022 Linux Zen kernel 5.18.3-zen1-1-zen
